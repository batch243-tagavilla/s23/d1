"use strict";

// [Section] Object

    /* 
        - An object is a data type that is used to represent real world objects.
        - it is a collection of related data and/or functionalities/method.
        - information stored in objects are represented in a "key:value" pair.
        - key is also mostly referred to as "property" of an object.
        - different data type may be stored in an object's property creating data structures.
    */

    // Creating objects using object initializers / literal notation

        const cellphone = {
            name: "Nokia 3310",
            manufactureDate: 1999
        };

        console.log("Result from creating using literal notation: ");
        console.log(cellphone);
    
    // Creating objects using constructor function
        /* 
            - creates a reusable function to create several objects that have the same data structure
            - this is useful for create multiple instances/copies of an object
            - an instance is a concrete occurence of any object which emphasize distinct/unique identity of it    
        */

        function Laptop(name, manufactureDate) {
            this.name = name;
            this.manufactureDate = manufactureDate;
        }

        /* Instantiation */
        // the "new" operator creates an instances of an object
        // objects and instances are often interchange because object literals(let object = {}) and instances (let objectName = new functionName(arguments)) are distinct/unique objects

        const laptop1 = new Laptop("Lenovo", 2008);
        console.log("Result from creating objects using object constructor: ");
        console.log(laptop1);

        const laptop2 = new Laptop("MacBook Air", 2020);
        console.log("Result from creating objects using object constructor: ");
        console.log(laptop2);

        /* Mini-Activity */
        function Menu(name, price) {
            this.menuName = name;
            this.menuPrice = price;
        }

        const mealOne = new Menu("Breakfast", 299);
        console.log(mealOne);

        // Creating empty objects
        const laptop3 = {};
        const myComputer = new Object();
        console.log(laptop3);
        console.log(myComputer);

        // Accessing objects inside an array;
        let array = [laptop1, laptop2];
        console.log(array);

        /* bracket notation */
        console.log(array[0]["name"]);

        /* dot notation */
        console.log(array[0].manufactureDate);

// [Section] Initializing/Adding/Deleting/Reassigning Object properties

    /* 
        - like any other variable in JavaScript, objects have their properties initialized/aded after the object was created/declared.
    */

    let car = {};
    console.log(car);

    // Initializing/Adding/Reassigning object properties using dot notation
    car.name = "Honda Civic";
    console.log(car);

    // Initializing/Adding object properties using bracket notation
    car["manufactureDate"] = 2019;
    console.log(car);

    // Deleting object properties using bracket notation
    /* delete car["name"];
    console.log(car); */

    // Deleting object properties using dot notation
    delete car.manufactureDate;
    console.log(car);

// [Section] Object Methods

    /* 
        - a method is a function which is a property of an object
        - they are also functions and one of the key differences they have is that methods re functions related to a specific object
    */
   
    let person = {
        name: "John",
        talk: function() {
            console.log("Hello my name is " + this.name);
        }
    }

    console.log(person);
    person.talk();

    // Add method to objects
    person.walk = function() {
        console.log(this.name + " walked 25 steps forwards.");
    }

    person.walk();

    let friends = {
        firstName: "Joe",
        lastName: "Smith",
        address: {
            city: "Austin",
            country: "Texas"
        },
        phoneNumber: [["09123456789"], ["043-4214-321"]],
        emails: ["joe@gmail.com", "joesmith@email.xyz"],
        introduce: function() {
            console.log("Hello my name is " + this.firstName + " " + this.lastName + ". I live in " + this.address.city + ", " + this.address.country +
            ". My emails are " + this.emails[0] + " and " + this.emails[1] + ". My numbers are " + this.phoneNumber[0][0] + " and " + this.phoneNumber[1][0] + ".");
        }
    }

    friends.introduce();

    // create an object constructor

    function Pokemon(name, level) {
        this.pokemonName = name;
        this.pokemonLevel = level;
        this.pokemonHealth = 2*level;
        this.pokemonAttack = level;

        this.tackle = function(target) {
            console.log(this.pokemonName + " tackles " + target.pokemonName);
            console.log(target.pokemonName + "'s health is now reduced to " + this.pokemonHealth);
        }

        this.faint = function() {
            console.log(this.pokemonName + " fainted!");
        }
    }

    let pikachu = new Pokemon("Pikachu", 12);
    console.log(pikachu);
    let gyarados = new Pokemon("Gyarados", 20);
    console.log(gyarados);
    pikachu.tackle(gyarados);